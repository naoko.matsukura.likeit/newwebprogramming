package controller;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import dao.UserDao;
import model.User;
import util.PasswordEncorder;

/** Servlet implementation class LoginServlet */
@WebServlet("/LoginServlet")
public class LoginServlet extends HttpServlet {
  private static final long serialVersionUID = 1L;

  /** @see HttpServlet#HttpServlet() */
  public LoginServlet() {
    super();
  }

  /** @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response) */
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {

    /**** ログインの有無確認 start ****/
    // ログイン情報を取得
    HttpSession session = request.getSession();
    User user = (User) session.getAttribute("userInfo");

    // ログイン情報があればユーザー一覧へ遷移
    if (user != null) {
      response.sendRedirect("UserListServlet");
      return;
    }
    /**** ログインの有無確認 end ****/

    // フォワード
    RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userLogin.jsp");
    dispatcher.forward(request, response);
  }

  /** @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response) */
  protected void doPost(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    // リクエストパラメータの文字コードを指定
    request.setCharacterEncoding("UTF-8");

    UserDao userDao = new UserDao();

    // リクエストパラメータの入力項目を取得
    String loginId = request.getParameter("user-loginid");
    String password = request.getParameter("password");

    // 入力されたパスワードを暗号化
    String encorderdPassword = PasswordEncorder.encordPassword(password);
    // パスワード(暗号化後)とログインIDでユーザー情報を検索
    User user = userDao.findByLoginInfo(loginId, encorderdPassword);

    /**** テーブルに該当のデータが見つからなかった場合 ****/
    if (user == null) {
      // リクエストスコープにエラーメッセージをセット
      request.setAttribute("errMsg", "ログインIDまたはパスワードが異なります");
      request.setAttribute("loginId", loginId);

      // ログインjspにフォワード
      RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userLogin.jsp");
      dispatcher.forward(request, response);
      return;
    }

    /**** テーブルに該当のデータが見つかった場合 ****/
    // セッションにユーザの情報をセット
    HttpSession session = request.getSession();
    session.setAttribute("userInfo", user);

    // ユーザ一覧のサーブレットにリダイレクト
    response.sendRedirect("UserListServlet");
  }
}


package controller;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import dao.UserDao;
import model.User;

/** Servlet implementation class UserDaleteServlet */
@WebServlet("/UserDaleteServlet")
public class UserDaleteServlet extends HttpServlet {
  private static final long serialVersionUID = 1L;

  /** @see HttpServlet#HttpServlet() */
  public UserDaleteServlet() {
    super();
  }

  /** @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response) */
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    /**** ログインの有無確認 start ****/
    HttpSession session = request.getSession();
    User loginUser = (User) session.getAttribute("userInfo");

    // ログイン情報がない場合
    if (loginUser == null) {
      // セッションがなかったらログイン画面へ
      response.sendRedirect("LoginServlet");
      return;
    }
    /**** ログインの有無確認 end ****/

    // URLパラメータからidを取得
    int id = Integer.valueOf(request.getParameter("id"));

    // idに紐づくユーザー情報を取得
    User user = new UserDao().findById(id); // 何度も使わないなら変数に展開する必要ない
    request.setAttribute("user", user);

    // 削除画面へ遷移
    RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userDelete.jsp");
    dispatcher.forward(request, response);
    return;
  }

  /** @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response) */
  protected void doPost(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    // idを取得
    String id = request.getParameter("user-id");

    UserDao dao = new UserDao();

    // 削除処理
    dao.delete(Integer.valueOf(id));
    // ユーザー一覧へ遷移
    response.sendRedirect("UserListServlet");
    return;
  }
}



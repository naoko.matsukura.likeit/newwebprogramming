package controller;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import dao.UserDao;
import model.User;
import util.PasswordEncorder;

/** Servlet implementation class UserAdd */
@WebServlet("/UserAddServlet")
public class UserAddServlet extends HttpServlet {
  private static final long serialVersionUID = 1L;

  /** @see HttpServlet#HttpServlet() */
  public UserAddServlet() {
    super();
  }

  /**
   * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
   */
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {

    /**** ログインの有無確認 start ****/
    HttpSession session = request.getSession();
    User user = (User) session.getAttribute("userInfo");

    // ログイン情報がない場合
    if (user == null) {
      // セッションがなかったらログイン画面へ
      response.sendRedirect("LoginServlet");
      return;
    }
    /**** ログインの有無確認 end ****/

    // 新規登録のjspにフォワード
    RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userAdd.jsp");
    dispatcher.forward(request, response);
    }

  /**
   * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
   */
  protected void doPost(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {

    // リクエストパラメータの文字コードを指定
    request.setCharacterEncoding("UTF-8");

    UserDao userDao = new UserDao();

    // 新規登録画面で入力された情報を取得
    String loginId = request.getParameter("user-loginid");
    String password = request.getParameter("password");
    String confirmPassword = request.getParameter("password-confirm");
    String name = request.getParameter("user-name");
    String birthdate = request.getParameter("birth-date");

    /**** 新規登録画面に未入力があった or パスワードが違う or login_idが既に登録されている 場合 ****/
    if (!isInputAll(loginId, password, confirmPassword, name, birthdate)
        || !(password.equals(confirmPassword)) || userDao.existUser(loginId)) {


      // エラーメッセージ
      request.setAttribute("errMsg", "入力された内容は正しくありません");

      // 入力内容を画面に表示するために、リクエストスコープに値をセットしておく
      request.setAttribute("inputloginId", loginId);
      request.setAttribute("name", name);
      request.setAttribute("birthdate", birthdate);

      // 新規登録画面へ遷移
      RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userAdd.jsp");
      dispatcher.forward(request, response);
      return;
    }

    /**** 新規登録成功時 ****/
    // パスワードを暗号化
    String encordedPassword = PasswordEncorder.encordPassword(password);

    // 登録処理
    userDao.insert(loginId, name, birthdate, encordedPassword);
    response.sendRedirect("UserListServlet");
    }

  /**
   * 新規登録画面の入力された値が全て入力されているかどうか
   *
   * @param loginId
   * @param password
   * @param confirmPassword
   * @param name
   * @param birthdate
   * @return
   */
  private static boolean isInputAll(String loginId, String password, String confirmPassword,
      String name, String birthdate) {


    return !(loginId.equals("") || password.equals("") || confirmPassword.equals("")
        || name.equals("") || birthdate.equals(""));


  }
}


package controller;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import dao.UserDao;
import model.User;

/** Servlet implementation class UserDetailServlet */
@WebServlet("/UserDetailServlet")
public class UserDetailServlet extends HttpServlet {
  private static final long serialVersionUID = 1L;

  /** @see HttpServlet#HttpServlet() */
  public UserDetailServlet() {
    super();
  }

  /** @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response) */
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {

    /**** ログインの有無確認 start ****/
    HttpSession session = request.getSession();
    User loginUser = (User) session.getAttribute("userInfo");

    // ログイン情報がない場合
    if (loginUser == null) {
      // セッションがなかったらログイン画面へ
      response.sendRedirect("LoginServlet");
      return;
    }
    /**** ログインの有無確認 end ****/

    // GETパラメータのIDを取得
    int id = Integer.valueOf(request.getParameter("id"));

    UserDao dao = new UserDao();
    // IDに紐づく情報を取得
    User user = dao.findById(id);

    request.setAttribute("user", user);

    // 詳細画面へ
    RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userDetail.jsp");
    dispatcher.forward(request, response);
    return;
  }
}
CREATE TABLE user (
id SERIAL PRIMARY KEY UNIQUE NOT NULL AUTO_INCREMENT,
login_id varchar(255) UNIQUE NOT NULL,
name varchar(255) NOT NULL,
birth_date DATE NOT NULL,
password varchar(255) NOT NULL,
is_admin boolean NOT NULL default false,
create_date DATETIME NOT NULL,
update_date DATETIME NOT NULL
);



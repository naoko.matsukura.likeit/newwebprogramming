CREATE DATABASE usermanagement character set utf8;
USE usermanagement;
CREATE TABLE user (
id SERIAL PRIMARY KEY UNIQUE NOT NULL AUTO_INCREMENT,
login_id varchar(255) UNIQUE NOT NULL,
name varchar(255) NOT NULL,
birth_date DATE NOT NULL,
password varchar(255) NOT NULL,
is_admin boolean NOT NULL default false,
create_date DATETIME NOT NULL,
update_date DATETIME NOT NULL
);

USE usermanagement;
INSERT INTO user (login_id, name, birth_date, password, is_admin, create_date, update_date)
VALUES ('admin', '�Ǘ���', '1998-11-17', 'password', true, now(),now());
